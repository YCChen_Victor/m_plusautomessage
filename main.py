from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from PIL import Image # pip install pillow
import base64
import json
from functions.CaptchaFunction import captcha_script, IdentifyCaptchaNumber, CleanCaptcha
from docs.path import CaptchaImagePath
import matplotlib.pyplot as plt
import cv2 # pip3 install opencv-python
import time

# open website
driver = webdriver.Chrome()
driver.get("https://web.mplusapp.com/chat.do")

# find the captcha element
ele_captcha = driver.find_element_by_xpath("html/body/div[4]/div[3]/div[1]/div[1]/form/div[1]/div/div[3]/div[2]/div[2]/img")

# get the captcha as a base64 string
img_captcha_base64 = driver.execute_async_script(captcha_script, ele_captcha)

# save the captcha to a file
with open(CaptchaImagePath, 'wb') as f:
    f.write(base64.b64decode(img_captcha_base64))

# read captcha image
captcha_img = cv2.imread(CaptchaImagePath)
# plt.imshow(captcha_img)
# plt.show()

# identify captcha
cleaned_captcha = CleanCaptcha(captcha_img)
CaptchaNumber = IdentifyCaptchaNumber(cleaned_captcha)

# load email and password
with open('./docs/identity.json' , 'r') as reader:
    jf = json.loads(reader.read())
email = jf['email']
password = jf['code']

# input mail box
email_box = driver.find_element_by_id("acc")
try:
    email_box.send_keys(email)
except Exception as e:
    print(e)

# input password box
password_box = driver.find_element_by_id("pwd1").click()
password_box = driver.find_element_by_id("pwd")
try:
    password_box.send_keys(password)
except Exception as e:
    print(e)

# input verification code box
code_box = driver.find_element_by_id("f_ccha")
try:
    code_box.send_keys(CaptchaNumber)
except Exception as e:
    print(e)

# click login
driver.find_element_by_id("eSubmitBtn").click()

# click target user
# 在這邊，如果出現error，就要重新登入一次
time.sleep(10)
driver.find_elements_by_css_selector('.chatprev04007966')[0].click()

# input text
for i in range(1000):
    time.sleep(5)
    driver.find_elements_by_css_selector('.chatroomStyle02')[0].send_keys(i, Keys.ENTER)

"""
出現以下error selenium.common.exceptions.InvalidSelectorException: Message: invalid selector: An invalid or illegal selector was specified
  (Session info: chrome=81.0.4044.138)
"""
