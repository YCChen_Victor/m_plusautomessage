import numpy as np
import cv2
import matplotlib.pyplot as plt
from PIL import Image
import pytesseract

captcha_script = """
	    var ele = arguments[0], callback = arguments[1];
	    ele.addEventListener('load', function fn(){
	      ele.removeEventListener('load', fn, false);
	      var cnv = document.createElement('canvas');
	      cnv.width = this.width; cnv.height = this.height;
	      cnv.getContext('2d').drawImage(this, 0, 0);
	      callback(cnv.toDataURL('image/jpeg').substring(22));
	    }, false);
	    ele.dispatchEvent(new Event('load'));
	    """

def CleanCaptcha(image):

    # Convert BGR to HSV
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    # Threshold of yellow in HSV space 
    lower_yellow = np.array([22, 93, 0])
    upper_yellow = np.array([45, 255, 255])

    # Threshold the HSV image to get only yellow colors
    mask_yellow = cv2.inRange(hsv, lower_yellow, upper_yellow)

    # Bitwise-AND mask and original image
    res_yellow = cv2.bitwise_and(image, image, mask= mask_yellow)

    '''
    還是要想辦法把糊糊的地方刪掉
    一個方法可能是把比較淺的顏色拿掉
    不過應該是可以直接進入辨識環節
    '''

    return(res_yellow)

def IdentifyCaptchaNumber(CaptchaImage):
    text = pytesseract.image_to_string(CaptchaImage,lang='eng',config='--psm 6 digits')
    return text
